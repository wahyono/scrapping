from typing import Dict
from urllib.request import urlopen, Request
import json
import re
from bs4 import BeautifulSoup

url = 'https://metal-archives.com/browse/ajax-letter/l/A/json'
req = Request(url, headers={'User-Agent': 'Mozilla/5.0'})
response = urlopen(req)
responseDecode = response.read().decode('utf-8')
dumpJSON = json.dumps(responseDecode, ensure_ascii=False, separators=(',', ':'), indent=0).encode('utf8')
subs = re.sub('\s+',' ',responseDecode)

subs = subs.replace('[ ', '[')
subs = subs.replace(' ]', ']')
subs = subs.replace('{ ', '{')
subs = subs.replace(' }', '}')
subs = subs.replace('[ ', '[')
subs = subs.replace(' , ', ',')
subs = subs.replace('[ ', '[')
subs = subs.replace('", "', '","')
subs = subs.replace('"sEcho":,', '')

jsonLoad = json.loads(subs)

results = []

if len(jsonLoad['aaData']):
    for a in jsonLoad['aaData']:
        aBandName = BeautifulSoup(a[0], 'lxml')
        bandName = list(aBandName.stripped_strings)
        aStatus = BeautifulSoup(a[3], 'lxml')
        status = list(aStatus.stripped_strings)

        data = {}
        data['band_name'] = bandName[0];
        data['link'] = aBandName.a['href']
        data['origin'] = a[1]
        data['genre'] = a[2]
        data['status'] = status[0]

        results.append(data)

print(results)
print(str(len(results)))