from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
import pymongo
import sys

headers = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET',
    'Access-Control-Allow-Headers': 'Content-Type',
    'Access-Control-Max-Age': '3600',
    'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0'
}
timeout_in_seconds = 10
mongoClient = pymongo.MongoClient('mongodb://localhost:27017')
database = mongoClient['metal-archives']
coll = database['bands3']
alphabets = sys.argv[1]
limit = int(sys.argv[2])
global isFinish
isFinish = False

def extractData(html):
    collection = []
    soup = BeautifulSoup(html, 'lxml')
    tbody = soup.find('tbody')
    trs = tbody.find_all('tr')
    nCounter = 0

    for a in trs:
        tds = a.find_all('td')

        getData = coll.find_one({'link': tds[0].a['href']})

        if getData is not None:
            continue

        data = {}

        data['band_name'] = tds[0].get_text()
        data['origin'] = tds[1].get_text()
        data['genre'] = tds[2].get_text()
        data['status'] = tds[3].get_text()
        data['details'] = scrapDetails(tds[0].a['href'])
        data['link'] = tds[0].a['href']
        data['scrap_discography'] = False

        coll.insert_one(data)

        collection.append(data)

        print(data['band_name'])

        nCounter += 1

        if nCounter == limit:
            global isFinish
            isFinish = True
            break

    return collection


def scrapDetails(url):
    options = webdriver.ChromeOptions()
    options.add_argument('--headless')
    driver = webdriver.Chrome(chrome_options=options)
    driver.get(url)
    details = {}
    details['origin'] = ''
    details['location'] = ''
    details['status'] = ''
    details['formed_in'] = 0
    details['years_active'] = ''
    details['genre'] = ''
    details['lyrical_themes'] = ''
    details['current_label'] = ''
    details['discogs'] = []
    details['members'] = []

    try:
        element = WebDriverWait(driver, timeout_in_seconds).until(ec.presence_of_element_located((By.CLASS_NAME, 'discog')))
        html = driver.page_source
        soup = BeautifulSoup(html, 'lxml')

        bandStats = soup.find('div', {'id': 'band_stats'})
        bandStatsDl = bandStats.find_all('dl')
        bandStatsDdFirst = bandStatsDl[0].find_all('dd')
        bandStatsDdSecond = bandStatsDl[1].find_all('dd')
        bandStatsDdThird = bandStatsDl[2].find_all('dd')

        details['origin'] = bandStatsDdFirst[0].get_text()
        details['location'] = bandStatsDdFirst[1].get_text()
        details['status'] = bandStatsDdFirst[2].get_text()
        details['formed_in'] = bandStatsDdFirst[3].get_text()
        details['genre'] = bandStatsDdSecond[0].get_text()
        details['lyrical_themes'] = bandStatsDdSecond[1].get_text()
        details['current_label'] = bandStatsDdSecond[2].get_text()
        details['years_active'] = bandStatsDdThird[0].get_text().replace('\n', '')

        discogTable = soup.find('table', {'class': 'discog'})
        discogTbody = discogTable.find('tbody')
        discogTr = discogTbody.find_all('tr')

        for a in discogTr:
            tds = a.find_all('td')

            data = {}

            if len(tds) > 1:
                data['name'] = tds[0].get_text()
                data['type'] = tds[1].get_text()
                data['year'] = tds[2].get_text()
                data['link'] = tds[0].find('a').get('href')

            details['discogs'].append(data)

        memberTable = soup.find_all('table', {'class': 'lineupTable'})

        if len(memberTable) > 0:
            memberTbody = memberTable[0].find('tbody')
            memberTr = memberTbody.find_all('tr')

            for b in memberTr:
                if b.get('class')[0] == 'lineupRow':
                    tds = b.find_all('td')

                    data = {}
                    data['name'] = tds[0].get_text().replace('\n', '')
                    data['description'] = tds[1].get_text().replace('\n', '')

                    details['members'].append(data)
    finally:
        driver.quit()

    return details


def scrapSite(alp):
    print('crawl ' + alp)

    colls = []
    global isFinish
    nPage = 1
    url = 'https://www.metal-archives.com/lists/' + alp
    options = webdriver.ChromeOptions()
    options.add_argument('--headless')
    driver = webdriver.Chrome(chrome_options=options)
    driver.get(url)

    try:
        element = WebDriverWait(driver, timeout_in_seconds).until(ec.presence_of_element_located((By.CLASS_NAME, 'odd')))

        html = driver.page_source
        data = extractData(html)
        # colls = colls + data
        # coll.insert_many(data)
        isContinue = False

        print('page 1')

        try:
            nextEl = driver.find_elements(By.XPATH, '//a[contains(@class, "paginate_button") and text()="' + str(nPage+1) + '"]')

            if len(nextEl) > 0 and not isFinish:
                isContinue = True
            else:
                isContinue = False
        except NoSuchElementException  as e:
            isContinue = False
        except TimeoutException as t:
            isContinue = False

        while isContinue:
            nPage += 1
            driver.find_element_by_css_selector('.next.paginate_button').click()
            element = WebDriverWait(driver, timeout_in_seconds).until(ec.presence_of_element_located((By.XPATH, '//a[contains(@class, "paginate_active") and text()="' + str(nPage) + '"]')))
            newHtml = driver.page_source
            newData = extractData(newHtml)
            # coll.insert_many(newData)
            # colls = colls + newData

            print('page ' + str(nPage))

            try:
                nextEl = driver.find_elements(By.XPATH, '//a[contains(@class, "paginate_button") and text()="' + str(nPage+1) + '"]')

                if len(nextEl) > 0 and not isFinish:
                    isContinue = True
                else:
                    isContinue = False
            except NoSuchElementException  as e:
                isContinue = False
                break
            except TimeoutException as t:
                isContinue = False
                break

        # print(str(len(colls)))

        # if len(colls) > 0:
        #     coll.insert_many(colls)
    finally:
        driver.quit()


scrapSite(alphabets)